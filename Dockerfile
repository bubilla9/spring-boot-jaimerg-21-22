FROM openjdk:13-alpine
VOLUME /tmp
ADD /target/*.jar springbootgitlab-0.0.1-SANPSHOT.jar
ENTRYPOINT [ "java","-jar","/springbootgitlab-0.0.1-SANPSHOT.jar" ]